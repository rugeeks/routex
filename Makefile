#
#  Makefile for Go
#
export GOPATH :=$(PWD)
export PATH   := /usr/local/go/bin:$(PATH)

SHELL=/usr/bin/env bash
GOPATH=$(shell pwd)
VERSION=$(shell git describe --tags --always)

default: clean update protogen build

update:
	cd ./src/routeX && glide up

protogen:
	protoc --go_out=${GOPATH}/src/routeX/core/ proto/register/register.proto
	protoc --go_out=${GOPATH}/src/routeX/core/ proto/health/health.proto

build:
	go build -race -ldflags="-X main.Version=${VERSION}" -o bin/routeX-${VERSION} routeX

clean:
	rm -f bin/*
