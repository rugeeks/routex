package main

import (
	"github.com/neliseev/logger"
	"os"
	"os/signal"
	"routeX/core"
	"routeX/server"
	"syscall"
)

var log logger.Log // logger subsystem

////
// Main initialize core (configs, logger) and run daemon
func main() {
	// Core initialization
	config, err := core.New()
	if err != nil {
		log.Critf("Init Core: %v", err)
	}

	log.Infof("Starting RouteX server ver %v", core.Version)
	srv := new(server.RouteX)
	srv.Run(config.Server.DNS, config.Server.Proto)

	// Waiting Signals from OS
	sig := make(chan os.Signal)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	<-sig
	log.Info("Stopping RouteX")
}
