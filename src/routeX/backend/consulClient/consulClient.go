package consulClient

import (
	"github.com/hashicorp/consul/api"
	"github.com/neliseev/logger"
	"sync"
	"time"
)

var log *logger.Log

type Client struct {
	api api.Config
	sync.RWMutex
}

type Config struct {
	Address    string `json:",omitempty"` // Address is the address of the Consul server
	Datacenter string `json:",omitempty"` // Datacenter to use. If not provided, the default agent datacenter is used.
}

func (cc *Client) New(c *Config) *Client {
	cc.api.Address = c.Address
	cc.api.Datacenter = c.Datacenter

	return cc
}

func (cc *Client) connect() (*api.Client, error) {
	return api.NewClient(&cc.api)
}

func (cc *Client) Register(id, name string, tags []string, addr string, port int, ttl string) error {
	cc.RLock()
	defer cc.RUnlock()

	client, err := cc.connect()
	if err != nil {
		return err
	}
	agent := client.Agent()

	service := &api.AgentServiceRegistration{
		ID:                id,
		Name:              name,
		Tags:              tags,
		Port:              port,
		Address:           addr,
		EnableTagOverride: true,
		Check:             &api.AgentServiceCheck{TTL: ttl},
	}

	return agent.ServiceRegister(service)
}

func (cc *Client) UpdateTTL(checkID, output, status string) error {
	cc.RLock()
	defer cc.RUnlock()

	client, err := cc.connect()
	if err != nil {
		return err
	}
	agent := client.Agent()

	return agent.UpdateTTL(checkID, output, status)
}

func (cc *Client) SearchByGEO(service, cityName, countryID string) ([]*api.ServiceEntry, error) {
	cc.RLock()
	defer cc.RUnlock()

	client, err := cc.connect()
	if err != nil {
		return nil, err
	}
	agent := client.Health()

	qopt := &api.QueryOptions{
		Datacenter:        cc.api.Datacenter,
		AllowStale:        true,
		RequireConsistent: false,
		WaitIndex:         uint64(5),
		WaitTime:          time.Second * 2,
	}

	services, meta, err := agent.Service(service, cityName, true, qopt)
	if err != nil {
		return nil, err
	}
	log.Debugf("Search in consul, service: %s, city level: %s, results: %+v, meta: %+v", service, cityName, services, meta)

	// ToDo need refactoring it to closure
	if len(services) == 0 {
		services, meta, err := agent.Service(service, countryID, true, qopt)
		if err != nil {
			return nil, err
		}
		log.Debugf("Search in consul, service: %s, county level: %s, results: %+v, meta: %+v", service, countryID, services, meta)

		if len(services) == 0 {
			services, meta, err := agent.Service(service, "", true, qopt)
			if err != nil {
				return nil, err
			}
			log.Debugf("Search in consul, service: %s, global level, results: %+v, meta: %+v", service, services, meta)

			return services, nil
		}
	}

	return services, nil
}
