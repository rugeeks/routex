package server

import (
	"github.com/miekg/dns"
	"github.com/neliseev/logger"
	"github.com/neliseev/socket"
	"routeX/server/nameserver"
	"routeX/server/protoserver"
	"strings"
)

// logger subsystem
var log logger.Log

type RouteX struct {
	Nameserver  *nameserver.Server
	Protoserver *protoserver.Server
}

// Run Starting RouteX Server
func (srv *RouteX) Run(nsCfg *nameserver.Config, protoserverCfg *protoserver.Config) {
	////
	// Starting Proto Server
	////

	// Start UDP Proto Server
	if protoserverCfg.ListenUDP != "" {
		s := protoserver.New(protoserverCfg.ListenUDP, "udp")
		srv.Protoserver = s
		go s.ListenAndServe()
		log.Infof("Proto server started on: %s %s", protoserverCfg.ListenUDP, "udp")
	}

	// Start TCP Proto Server
	if protoserverCfg.ListenTCP != "" {
		s := protoserver.New(protoserverCfg.ListenTCP, "tcp")
		srv.Protoserver = s
		go s.ListenAndServe()
		log.Infof("Proto server started on: %s %s", protoserverCfg.ListenTCP, "tcp")
	}

	// Register Proto handlers
	socket.HandleFunc("register", protoserver.RegisterHandler)
	socket.HandleFunc("health", protoserver.HealthHandler)

	////
	// Starting Nameserver
	////

	// Register DNS handlers
	dns.HandleFunc(nsCfg.Domain, nameserver.Dispatcher)

	// Check inputs
	// If FQDN don't have at end ".", add them
	if !strings.HasSuffix(nsCfg.FQDN, ".") {
		nsCfg.FQDN += "."
	}
	if !strings.HasSuffix(nsCfg.Domain, ".") {
		nsCfg.Domain += "."
	}

	// Start UDP DNS Server
	if nsCfg.ListenUDP != "" {
		s := nameserver.New(nsCfg.ListenUDP, "udp", nsCfg.FQDN, nsCfg.Secret)
		go s.ListenAndServe()
		srv.Nameserver = s
		log.Infof("UDP NS server started on: %s %s", nsCfg.ListenTCP, "udp")
	}

	// Start TCP DNS Server
	if nsCfg.ListenTCP != "" {
		s := nameserver.New(nsCfg.ListenTCP, "tcp", nsCfg.FQDN, nsCfg.Secret)
		go s.ListenAndServe()
		srv.Nameserver = s
		log.Infof("TCP NS server started on: %s %s", nsCfg.ListenTCP, "tcp")
	}
}
