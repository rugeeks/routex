package protoserver

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/neliseev/socket"
	"routeX/backend/consulClient"
	"routeX/core/proto/health"
)

func HealthHandler(w socket.Response, r *socket.Msg) {
	// Prepare response
	m := new(socket.Msg)
	m.Req = r.Req

	msg := new(health.HealthReq)
	if err := proto.Unmarshal(r.Data, msg); err != nil {
		log.Errf("Can't deserealise proto messgae: %s,", err)
	}
	log.Debugf("Incomming call: %s, DATA: %+v", r.Req, msg)

	consul := new(consulClient.Client)
	checkID := fmt.Sprintf("service:%v", msg.Seed)
	if err := consul.UpdateTTL(checkID, "OK", "pass"); err != nil {
		log.Errf("Consul update health check for checkID: %v, error: %s", checkID, err)

		resp, err := proto.Marshal(&health.HealthResp{State: false, Msg: fmt.Sprint(err)})
		if err != nil {
			log.Errf("Can't marshal failed response: %s", err)
		}

		m.Data = append(m.Data, resp...)
		if err := w.WriteMsg(m); err != nil {
			log.Errf("Can't write response: %s", err)
		}

		return
	}

	// Prepared OK MSG
	respMSG, err := proto.Marshal(&health.HealthResp{State: true, Msg: "Success"})
	if err != nil {
		log.Err(err)
	}

	m.Data = respMSG
	if err := w.WriteMsg(m); err != nil {
		log.Errf("Can't write response: %s", err)
	}

	return
}
