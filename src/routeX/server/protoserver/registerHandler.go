package protoserver

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/neliseev/socket"
	"net"
	"routeX/backend/consulClient"
	"routeX/core/proto/register"
	"routeX/core/search/geo"
)

type service struct {
	id   string
	name string
	tags []string
	addr string
	port int
	ttl  string
}

func RegisterHandler(w socket.Response, r *socket.Msg) {
	// Prepare response
	m := new(socket.Msg)
	m.Req = r.Req

	msg := new(register.RegisterSEEDReq)
	if err := proto.Unmarshal(r.Data, msg); err != nil {
		log.Errf("Can't deserealise proto messgae: %s,", err)

		return
	}
	log.Debugf("Incomming call: %s, DATA: %+v", r.Req, msg)

	// Convert net.Addr to net.IP
	var ip net.IP
	switch w.RemoteAddr().Network() {
	case "tcp":
		ip = w.RemoteAddr().(*net.TCPAddr).IP
	case "udp":
		ip = w.RemoteAddr().(*net.UDPAddr).IP
	default:
		log.Errf("Can't get connection type: %s", w.RemoteAddr().Network())
	}

	// Get GEO Tags from IP
	geoTags := new(geo.Tags)
	geoTags.GetGEOTags(ip)

	var tags []string
	tags = append(tags, geoTags.Country)
	tags = append(tags, geoTags.City)

	s := &service{
		id:   msg.Seed,
		name: msg.Client,
		addr: ip.String(),
		ttl:  msg.CheckTTL,
		tags: tags,
	}
	if err := registerService(s); err != nil {
		log.Errf("Consul register service: %v", err)

		resp, err := proto.Marshal(&register.RegisterSEEDResp{State: false, Msg: fmt.Sprint(err)})
		if err != nil {
			log.Errf("Can't marshal failed response: %s", err)
		}
		m.Data = append(m.Data, resp...)

		if err := w.WriteMsg(m); err != nil {
			log.Errf("Can't write response: %s", err)
		}

		return
	}

	resp, err := proto.Marshal(&register.RegisterSEEDResp{State: true, Msg: "Service " + msg.Seed + " registered success"})
	if err != nil {
		log.Errf("Can't marshal ok response: %s", err)
	}
	m.Data = append(m.Data, resp...)
}

func registerService(s *service) error {
	log.Debugf("Consul Register: %s", s)
	consul := new(consulClient.Client)
	if err := consul.Register(s.id, s.name, s.tags, s.addr, s.port, s.ttl); err != nil {
		return err
	}

	return nil
}
