package protoserver

import (
	"github.com/neliseev/logger"
	"github.com/neliseev/socket"
	"sync"
)

// logger subsystem
var log logger.Log

type Server struct {
	Socket *socket.Server
	sync.RWMutex
}

type Config struct {
	ListenUDP string `json:"listenUDP"` // ListenUDP ip:port for UDP
	ListenTCP string `json:"listenTCP"` // ListenTCP ip:port for TCP
}

func New(addr string, proto string) *Server {
	ps := new(Server)

	ps.Socket = &socket.Server{
		Addr:  addr,
		Proto: proto,
	}

	return ps
}

func (ps *Server) ListenAndServe() error {
	ps.Lock()
	defer ps.Unlock()

	if err := ps.Socket.ListenAndServe(); err != nil {
		return err
	}

	return nil
}
