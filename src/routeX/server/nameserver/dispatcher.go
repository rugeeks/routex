package nameserver

import (
	"github.com/miekg/dns"
	"time"
)

const compress bool = true

func Dispatcher(w dns.ResponseWriter, r *dns.Msg) {
	m := new(dns.Msg)
	m.SetReply(r)
	m.Compress = compress

	switch r.Question[0].Qtype {
	case dns.TypeTXT:
		rr, err := handleTXT(w, r)
		if err != nil {
			log.Errf("Resolving TXT: %s", err)
		}
		m.Answer = append(m.Answer, rr)
	default:
		fallthrough
	case dns.TypeA:
		rr, err := handleA(w, r)
		if err != nil {
			log.Errf("Resolving A: %s", err)
		}
		m.Answer = append(m.Answer, rr)
	case dns.TypeAAAA:
		rr, err := handleAAAA(w, r)
		if err != nil {
			log.Errf("Resolving AAAA: %s", err)
		}
		m.Answer = append(m.Answer, rr)
	case dns.TypeAXFR, dns.TypeIXFR:
		if err := handleAXFR(w, r); err != nil {
			log.Errf("Resolving AXFR: %s")
		}
		return
	}

	if r.IsTsig() != nil {
		if w.TsigStatus() == nil {
			m.SetTsig(r.Extra[len(r.Extra)-1].(*dns.TSIG).Hdr.Name, dns.HmacMD5, 300, time.Now().Unix())
		} else {
			log.Warnf("TSIG Status: %s", w.TsigStatus().Error())
		}
	}

	//////
	//// set TC when question eq (EXAMPLE)
	//if m.Question[0].Name == "test.rugeeks.com" {
	//	m.Truncated = true
	//	// send half a message
	//	buf, _ := m.Pack()
	//	w.Write(buf[:len(buf)/2])
	//	return
	//}

	if err := w.WriteMsg(m); err != nil {
		log.Errf("Can't write response: %s", err)
	}
}
