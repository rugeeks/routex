package nameserver

import (
	"github.com/miekg/dns"
	"net"
)

func handleTXT(w dns.ResponseWriter, r *dns.Msg) (rr dns.RR, err error) {
	var v4 bool   // IPv4 boolean
	var ra net.IP // Remote Address

	if ip, ok := w.RemoteAddr().(*net.UDPAddr); ok {
		ra = ip.IP
		v4 = ra.To4() != nil
	}
	if ip, ok := w.RemoteAddr().(*net.TCPAddr); ok {
		ra = ip.IP
		v4 = ra.To4() != nil
	}

	if v4 {
		rr = &dns.A{
			Hdr: dns.RR_Header{Name: "test.dom.", Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: 0},
			A:   net.ParseIP("8.8.8.8"),
		}
	} else {
		rr, err = handleAAAA(w, r)
		if err != nil {
			return nil, err
		}
	}

	return rr, nil
}
