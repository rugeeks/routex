package nameserver

import (
	"github.com/miekg/dns"
	"github.com/neliseev/logger"
	"sync"
)

var log logger.Log // logger subsystem

type Server struct {
	DNS *dns.Server
	sync.RWMutex
}

type Config struct {
	ListenUDP string `json:"listenUDP"`  // ip:port for UDP
	ListenTCP string `json:"listenTCP"`  // ip:port for TCP
	FQDN      string `json:",omitempty"` // FQDN name of current node
	Domain    string `json:"domain"`     // Domain name
	Secret    string `json:"secret"`     // TSIG HMAC-MD5 Secret, for generation use ldns-keygen -a hmac-md5 -b 256 -k rx-78.rugeeks.com
}

func New(addr, net, fqdn, secret string) *Server {
	ns := new(Server)

	ns.DNS = &dns.Server{
		Addr:       addr,
		Net:        net,
		TsigSecret: map[string]string{fqdn: secret},
	}

	return ns
}

func (ns *Server) ListenAndServe() error {
	ns.Lock()
	defer ns.Unlock()

	if err := ns.DNS.ListenAndServe(); err != nil {
		return err
	}

	return nil
}
