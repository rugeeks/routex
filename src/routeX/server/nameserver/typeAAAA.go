package nameserver

import (
	"github.com/miekg/dns"
	"net"
)

func handleAAAA(w dns.ResponseWriter, r *dns.Msg) (rr dns.RR, err error) {
	// ToDo Add GEO Based Search
	//var ra net.IP // Remote Address
	//
	//if ip, ok := w.RemoteAddr().(*net.UDPAddr); ok {
	//	ra = ip.IP
	//	ra.To4()
	//}
	//if ip, ok := w.RemoteAddr().(*net.TCPAddr); ok {
	//	ra = ip.IP
	//	ra.To4()
	//}

	rr = &dns.AAAA{
		Hdr:  dns.RR_Header{Name: "test.dom.", Rrtype: dns.TypeAAAA, Class: dns.ClassINET, Ttl: 0},
		AAAA: net.ParseIP("7.7.7.7"),
	}

	return rr, nil
}
