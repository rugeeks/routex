package nameserver

import (
	"github.com/miekg/dns"
	"net"
	"routeX/core/search"
)

func handleA(w dns.ResponseWriter, r *dns.Msg) (rr dns.RR, err error) {
	var v4 bool   // IPv4 boolean
	var ra net.IP // Remote Address

	if ip, ok := w.RemoteAddr().(*net.UDPAddr); ok {
		ra = ip.IP
		v4 = ra.To4() != nil
	}
	if ip, ok := w.RemoteAddr().(*net.TCPAddr); ok {
		ra = ip.IP
		v4 = ra.To4() != nil
	}

	if v4 {
		resp := new(search.Response)
		if err := resp.SeedSearch(r.Question[0].Name, ra); err != nil {
			return nil, err
		}

		rr = &dns.A{
			Hdr: dns.RR_Header{Name: resp.Name, Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: resp.TTL},
			A:   resp.IP,
		}
	} else {
		rr, err = handleAAAA(w, r)
		if err != nil {
			return nil, err
		}
	}

	return rr, nil
}
