package nameserver

import (
	"github.com/miekg/dns"
	"net"
)

func handleAXFR(w dns.ResponseWriter, r *dns.Msg) (err error) {
	c := make(chan *dns.Envelope)
	tr := new(dns.Transfer)
	defer close(c)
	if err := tr.Out(w, r, c); err != nil {
		return err
	}
	t := &dns.TXT{
		Hdr: dns.RR_Header{Name: "test.dom.", Rrtype: dns.TypeTXT, Class: dns.ClassINET, Ttl: 0},
		Txt: []string{"Example TXT RECORD"},
	}
	rr := &dns.A{
		Hdr: dns.RR_Header{Name: "test.dom.", Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: 0},
		A:   net.ParseIP("8.8.8.8"),
	}
	soa, _ := dns.NewRR(`whoami.clinet.rugeeks.com. 0 IN SOA linode.atoom.net. miek.miek.nl. 2009032802 21600 7200 604800 3600`)
	c <- &dns.Envelope{RR: []dns.RR{soa, t, rr, soa}}
	w.Hijack()

	return nil
}
