package cfg

import (
	"encoding/json"
	"flag"
	"github.com/neliseev/logger"
	"io/ioutil"
	"routeX/backend/consulClient"
	"routeX/server/nameserver"
	"routeX/server/protoserver"
)

// Config file structures
type Config struct {
	General ConfigGeneral
	Server  struct {
		DNS   *nameserver.Config
		Proto *protoserver.Config
	}
	Backend struct {
		Consul *consulClient.Config
	}
	Log *logger.Log
}

type ConfigGeneral struct {
	User    string `json:",omitempty"`
	Group   string `json:",omitempty"`
	Pid     string `json:",omitempty"`
	Workers int    `json:",omitempty"`
	ID      string `json:"id"`
	Name    string `json:"name"`
	FQDN    string `json:"fqdn"`
	Region  string `json:"region"`
	Default bool   `json:",omitempty"`
}

// Build config as Type Config from json and set env variables for AWS
func (c *Config) Build() error {
	// Command line params parsing
	var configFilePtr = flag.String("config", "/etc/routeX/server.json", "Path to config file")

	flag.Parse()

	// Read config file
	fh, err := ioutil.ReadFile(*configFilePtr)
	if err != nil {
		return err
	}

	// Parse config file to Config Type
	if err := json.Unmarshal(fh, &c); err != nil {
		return err
	}

	return nil
}
