package search

import (
	"errors"
	"net"
	"routeX/backend/consulClient"
	"routeX/core/search/geo"
	"math/rand"
)

type Response struct {
	Name string
	TTL  uint32
	IP   net.IP
}

func (r *Response) SeedSearch(service string, ip net.IP) error {
	r.Name = service
	r.TTL = 60

	geoTags := new(geo.Tags)
	geoTags.GetGEOTags(ip)

	consul := new(consulClient.Client)
	results, err := consul.SearchByGEO(service, geoTags.City, geoTags.Country)
	if err != nil {
		return err
	}

	l := len(results)

	if l == 0 {
		results, err := consul.SearchByGEO("default", geoTags.City, geoTags.Country)
		if err != nil {
			return err
		}

		l := len(results)

		if l == 0 {
			return errors.New("Can't find IP in backend")
		}

		rnd := rand.Intn(l)
		r.IP = net.ParseIP(results[rnd].Service.Address)

		return nil
	}

	rnd := rand.Intn(l)
	r.IP = net.ParseIP(results[rnd].Service.Address)

	return nil
}
