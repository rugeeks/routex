package geo

import (
	"github.com/oschwald/geoip2-golang"
	"net"
)

type Tags struct {
	Country string
	City    string
}

func (t *Tags) GetGEOTags(ip net.IP) error {
	// Get GEO Tags from IP
	db, err := geoip2.Open("./usr/local/lib/geoip/GeoLite2-City.mmdb")
	if err != nil {
		return err
	}
	defer db.Close()

	data, err := db.City(ip)
	if err != nil {
		return err
	}

	t.Country = data.Country.IsoCode
	t.City = data.City.Names["en"]

	return nil
}
