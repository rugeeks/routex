package core

import (
	"fmt"
	"routeX/backend/consulClient"
	"time"
)

func HealthMonitoring(c *consulClient.Config, id string) {
	for {
		log.Debug("Check internal health - started")
		// ToDo add testing methods for Redis and internal DNS
		consul := new(consulClient.Client)
		consul.New(c)
		if err := consul.UpdateTTL(fmt.Sprintf("service:%s", id), "test", "pass"); err != nil {
			log.Err(err)

			continue
		}
		log.Debug("Check internal health - passed")

		time.Sleep(60 * time.Second)
	}
}
