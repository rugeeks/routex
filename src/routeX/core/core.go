package core

import (
	"fmt"
	"github.com/neliseev/logger"
	"routeX/backend/consulClient"
	"routeX/core/cfg"
	"strconv"
	"strings"
)

var Version string = "0.2.0"

const ttl = "60s"

var log logger.Log // logger subsystem

// New - Building new config copy structure and initializing log system
// Return config type or error
func New() (*cfg.Config, error) {
	c := new(cfg.Config)

	// Building config
	if err := c.Build(); err != nil {
		if err := logger.New(&logger.Log{LogLevel: logger.Level}); err != nil {
			panic(err)
		}

		return nil, err
	}

	if c.Server.DNS.FQDN == "" {
		c.Server.DNS.FQDN = c.General.FQDN
	}

	// Initialization log system
	if err := logger.New(c.Log); err != nil {
		if err := logger.New(&logger.Log{LogLevel: logger.Level}); err != nil {
			panic(err)
		}

		return nil, err
	}

	// Setting log level and Version
	logger.Level = c.Log.LogLevel

	// Register self in Consul backend
	consul := new(consulClient.Client)
	consul.New(c.Backend.Consul)

	tags := []string{c.General.Region}
	if c.General.Default {
		tags = append(tags, "default")
	}

	// ToDo set configurable by config (if defined)
	idTCP := fmt.Sprintf("%s-%s-tcp", c.General.Name, c.General.ID)
	addrTCP := strings.Split(c.Server.Proto.ListenTCP, ":")
	portTCP, _ := strconv.ParseInt(addrTCP[1], 10, 64)
	tagTCP := append(tags, "tcp")
	if err := consul.Register(idTCP, c.General.Name, tagTCP, addrTCP[0], int(portTCP), ttl); err != nil {
		return nil, err
	}

	idUDP := fmt.Sprintf("%s-%s-udp", c.General.Name, c.General.ID)
	addrUDP := strings.Split(c.Server.Proto.ListenTCP, ":")
	portUDP, _ := strconv.ParseInt(addrTCP[1], 10, 64)
	tagUDP := append(tags, "udp")
	if err := consul.Register(idUDP, c.General.Name, tagUDP, addrUDP[0], int(portUDP), ttl); err != nil {
		return nil, err
	}

	// Start monitoring self
	go HealthMonitoring(c.Backend.Consul, idTCP)
	go HealthMonitoring(c.Backend.Consul, idUDP)

	return c, nil
}
