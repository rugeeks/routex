package main

import (
	"encoding/binary"
	"github.com/golang/protobuf/proto"
	"github.com/neliseev/logger"
	"net"
	"routeX/core/proto/health"
	"routeX/core/proto/register"
	"strconv"
	"time"
)

var log logger.Log // logger subsystem

type Packet struct {
	packetSize uint16 // full packet size max 65536 bytes
	headerSize uint16 // header size
	header     []byte // Header
	data       []byte // Data
}

// ToDo add more output logs for detailed debug
func main() {
	log.New()

	ProtoRegister := &register.RegisterSEEDReq{
		Seed:     "seed-v123456",
		Client:   "default",
		CheckTTL: "15s",
	}

	ProtoHealth := &health.HealthReq{
		Seed:   "seed-v123456",
		Client: "default",
		State:  true,
	}

	conn, err := net.Dial("tcp", "172.20.10.2:2117")
	if err != nil {
		log.Crit(err)
	}

	regMSG, err := proto.Marshal(ProtoRegister)
	if err != nil {
		log.Crit(err)
	}

	healthMSGOK, err := proto.Marshal(ProtoHealth)
	if err != nil {
		log.Err(err)
	}

	if err := writeToEdge(conn, "register", regMSG); err != nil {
		log.Critf("Can't register Node: %s", err)
	}
	conn.Close()

	time.Sleep(5 * time.Second)

	for {
		conn, err := net.Dial("tcp", "172.20.10.2:2117")
		if err != nil {
			log.Crit(err)
		}

		log.Info("Polling")

		if err := writeToEdge(conn, "health", healthMSGOK); err != nil {
			log.Err(err)
			conn.Close()

			break
		}

		conn.Close()

		time.Sleep(10 * time.Second)
	}

}

func writeToEdge(conn net.Conn, op string, data []byte) error {
	n, err := conn.Write(packetPack(op, data))
	if err != nil {
		return err
	}
	log.Infof("Sended: %s bytes", strconv.Itoa(n))

	return nil
}

func packetPack(op string, data []byte) []byte {
	// Preparing packet
	hdr := []byte(op)
	hdrSize := len(hdr)
	pktSize := hdrSize + len(data)

	pkt := &Packet{
		packetSize: uint16(2 + pktSize),
		headerSize: uint16(hdrSize),
		header:     hdr,
		data:       data,
	}

	// Creating packet with size 2 bytes total size + 2 bytes header size + size header + data
	var buf []byte = make([]byte, 4+pktSize)
	// Put total packet size
	offset := 0
	binary.BigEndian.PutUint16(buf[offset:], pkt.packetSize)
	// Put header size
	offset += 2
	binary.BigEndian.PutUint16(buf[offset:], pkt.headerSize)
	// Put header
	offset += 2
	copy(buf[offset:], hdr)
	// Put data
	offset += hdrSize
	copy(buf[offset:], pkt.data)

	return buf
}
