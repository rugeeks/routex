## Synopsis

Протокол управления между нодой и роутером поддерживает TCP и UDP
Формат сообщений Protocol Buffers

## API Reference
### Register Proto
Метод инициализации ноды, нужно вызывать каждый раз когда приложение запускается, в не зависимости от того, что это новая нода или старая.
Т.к. все ноды которые в течении 5 минут (конфигурируемое значение) не проявили признаков жизни, будут удалены из каталога дискавери.
При этом данные о файлах (в бд Redis) на ноде будут сохранены в течении жизни (TTL expires header)
### ss
## Reference

Каждое сообщени типа имеет префикс в конце запроса, пример RegisterEDGE_REQ.
- _Req - означает запрос в сервер RouteX
- _Resp - означет ответ от сервера RouteX

Сообщение которые посылают только управляющие команды, типа регистрации или обновления состояния ноды.
В ответ получают стандартное сообщение типа:

`ИмяМетода_Resp`

Формат стандартного ответа:

```
state bool      True - Ok, False - Err
error string    Сообщение ошибки
```


## License
RouteX
Copyright (C) 2016 Nikita Eliseev <n.eliseev@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
